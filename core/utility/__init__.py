import os
import json
from filelock import FileLock
import logging


def update_json_kv(path, key, value):
    lock = FileLock(f"{path}.lock")
    with lock:
        with open(path, 'a+')as f:
            f.seek(0)
            try:
                tmp_d = json.load(f)
#                logging.debug(f'Found json file with {str(tmp_d)}')
            except Exception as e:
                tmp_d = dict()
#                logging.debug(f'Error while loading json file {path}: {e}')
            try:
                tmp_d[key] = max(value, tmp_d[key])
            except KeyError:
                tmp_d[key] = value
            f.seek(0)
            f.truncate()
            json.dump(tmp_d, f, indent=2)
            f.flush()
            logging.debug(f'dumped {tmp_d} to {f}')


def get_cenv(env_str, default=None):
    return os.getenv(f'CUSTOM_ENV_{env_str}', default)
