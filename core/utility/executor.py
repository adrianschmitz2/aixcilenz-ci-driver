from core.modes.common import *

from core import utility


def async_process(file):
    return subprocess.Popen(f'tail -F {file}'.split())


class Executor(ABC):
    downscope_add = ""

    def __init__(self, job, downscope=False):
        self.job = job
        if downscope:
            self.downscope_add = f"sudo -u {self.job.account}"
        self.downscope = downscope

    # Executes internal management functions, e.g., setup scripts etc.
    @abstractmethod
    def management_handler(self, params="", wrapper_add="", script=""):
        pass

    # runs a script in the batch system with direct output
    @abstractmethod
    def run_direct(self, params="", wrapper_add="", script=""):
        pass

    # TODO: Use https://stackoverflow.com/questions/4417546/constantly-print-subprocess-output-while-process-is-running
    #       to capture and print live output
    def execute(self, helper_script='', allocator='', params='', wrapper_add='', pre_exec_scripts=[],
                target_script='', skip_env=False, run_async=False, main_script=False, install_env=False, **kwargs):
        if main_script:
            self.job.mode.custom_run_setup(main_script=main_script, run_async=run_async, **kwargs)
            logging.info(f'Executing with env: {str(self.job.custom_env)}')
        else:
            if install_env:
                params += f' --export=CUSTOM_SHELL_CONFIG={self.job.shell_config}'
        command = [helper_script]
        command.extend([f'{self.downscope_add} {allocator} {params} {wrapper_add}'])
        command.extend(pre_exec_scripts)
        command.append(target_script)
        logging.info(f'Executing command: {str(command)}')
        os.chdir('/tmp')
        main_proc = subprocess.Popen(command,
                                     env=(dict(os.environ, **{x: self.job.custom_env[x]
                                                              for x in self.job.custom_env}) if not skip_env
                                          else os.environ), **kwargs)
        side_proc = None
        if main_script and run_async:
            logging.debug(f'Starting inbetween processing')
            logging.debug(f'Reading from File: {self.job.mode.slurm_output_file}')
            side_proc = async_process(self.job.mode.slurm_output_file)
            time.sleep(10)
        stdout, stderr = main_proc.communicate()
        logging.debug(f'Finished main processing {main_proc.pid}')
        logging.debug(f'---stdout---\n {stdout}')
        logging.debug(f'---stderr---\n {stderr}')
        if main_script and run_async and side_proc:
            logging.debug(f'Terminating side_proc {side_proc.pid}')
            side_proc.terminate()
            logging.debug(f'Terminated side_proc {side_proc.pid}')
        job_was_canceled = get_cenv('CI_JOB_STATUS') == 'canceled'
        cmd_return_code = 1 if (int(main_proc.returncode) != 0 or job_was_canceled) else 0
        if main_script and (job_was_canceled or not self.job.allow_failure):
            utility.update_json_kv(self.job.error_code_file, self.job.jobid, cmd_return_code)
        if main_script and cmd_return_code != 0:
            if self.job.mode and not self.job.allow_failure:
                self.job.mode.cleanup_on_failure()
            print(subprocess.CompletedProcess(main_proc.args, main_proc.returncode, stdout, stderr).stdout)
            sys.exit(cmd_return_code)
        # FIXME: do not rely on internal implementation of subprocess.run
        return subprocess.CompletedProcess(main_proc.args, main_proc.returncode, stdout, stderr)


class Slurm_Executor(Executor, ABC):
    srun_path = "srun -Q --export=NONE"  # "/usr/local_host/bin/srun"
    sbatch_path = "sbatch"  # "/usr/local_host/bin/sbatch"
    salloc_path = "/opt/slurm/current/bin/salloc"
    scancel_path = "scancel"
    simple_job_id = ""

    def _get_slurm_cmd(self, base):
        add_args = ''
        constraint = get_cenv("SLURM_CONSTRAINT")
        if not constraint:
            constraint = get_cenv("SALLOC_CONSTRAINT")

        if constraint:
            add_args += f' --constraint={constraint}'

        return f'{base}{add_args}'

    def get_srun_cmd(self):
        return self._get_slurm_cmd(self.srun_path)

    def get_sbatch_cmd(self):
        return self._get_slurm_cmd(self.sbatch_path)

    def get_salloc_cmd(self):
        return self._get_slurm_cmd(self.salloc_path)

    def get_scancel_cmd(self):
        return self._get_slurm_cmd(self.scancel_path)

    def __init__(self, job, downscope=False):
        Executor.__init__(self, job, downscope=downscope)

    def get_default_params(self):
        return self.job.get_parameters(['SLURM_DPARAM'])

    def is_job_alive(self, job_id):
        srun_out = self.execute(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                allocator=self.get_srun_cmd(),
                                params=f"--jobid={job_id}",
                                wrapper_add=f'{self.job.shell_path} -l',
                                target_script=f"{self.job.scripts_path}/jobTester.sh",
                                text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
        logging.info(f"alive test output:{srun_out}")
        return srun_out == ""

    def set_internal_slurm_job(self):
        if not self.downscope:
            return

        if self.simple_job_id:
            if self.is_job_alive(self.simple_job_id):
                return

        try:
            with open(f"{self.job.runner_path}/SlurmIDMapping.json", "r") as file:
                mapping = json.loads(file.read())
                self.simple_job_id = mapping[get_cenv("CI_JOB_ID")]
                if self.is_job_alive(self.simple_job_id):
                    return
                else:
                    self.simple_job_id = None
                    logging.info("Old job expired, allocating new one.")
        except (IOError, KeyError, json.decoder.JSONDecodeError):
            self.simple_job_id = None
            logging.warning(f'Could not read internal Slurm jobid from mapping file')

        salloc_out = self.allocator(params=' '.join(['--cpus-per-task=1', '--ntasks=1', '--no-shell',
                                                              f'--job-name=CI_{self.job.pipeline_id}']))
        try:
            self.simple_job_id = re.search(r'salloc: job (\d+)', salloc_out).group(1)
            logging.info(f'Using internal Slurm jobid {self.simple_job_id}')
            man.add_id_mapping(f"{self.job.runner_path}/SlurmIDMapping.json", get_cenv("CI_JOB_ID"),
                               self.simple_job_id)
        except AttributeError:
            self.job.mode.abort(f'Could not allocate a Slurm job for internal usage\n'
                                f'runner_path={self.job.runner_path}'
                                f'\nallocator_out={salloc_out}')

    def allocator(self, params=""):
        logging.debug(f'allocating job for pipeline {self.job.pipeline_id}')
        salloc_out = self.execute(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                  allocator=self.get_salloc_cmd(),
                                  params=f'{self.get_default_params()} {params}',
                                  text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
        #print(salloc_out)
        logging.debug(f' salloc output: {salloc_out}')
        return salloc_out

    def management_handler(self, helper_script="", params="", wrapper_add="", script="", install_env=False, force_allocator=False):
        if helper_script == '':
            helper_script = f"{self.job.scripts_path}/runHelper.sh"

        if self.downscope:
            self.set_internal_slurm_job()
        management_out = self.execute(helper_script=helper_script,
                                      allocator=self.get_srun_cmd() if (self.downscope or force_allocator) else '',
                                      params=f'--jobid={self.simple_job_id} {self.get_default_params()} {params}' if (self.downscope or force_allocator) else '',
                                      wrapper_add=wrapper_add,
                                      #pre_exec_scripts=[self.job.shell_config],
                                      target_script=script, install_env=install_env,
                                      text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
        logging.debug(management_out)
        return management_out

    def cancel(self, jobid=""):
        if jobid == "" and self.downscope:
            self.set_internal_slurm_job()
            jobid = self.simple_job_id
        elif jobid == "" and not self.downscope:
            return '' 
        scancel_out = self.execute(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                   allocator=self.get_scancel_cmd(),
                                   params=f'{jobid}',
                                   text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
        logging.debug(f'scancel output: {scancel_out}')
        return scancel_out

    def run_direct(self, params="", wrapper_add="", script=""):
        self.execute(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                allocator=self.get_srun_cmd(),
                                params=f'{self.get_default_params()} {params}',
                                target_script=script,
                                wrapper_add=wrapper_add, main_script=True)
        return ''

    def run_batched(self, params="", wrapper_add="", script=""):
        sbatch_out = self.execute(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                  allocator=self.get_sbatch_cmd(),
                                  params=f'--export=NONE {self.get_default_params()} {params}',
                                  wrapper_add=wrapper_add, 
                                  target_script=script,
                                  main_script=True, run_async=True,
                                  text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
        logging.debug(f'sbatch output: {sbatch_out}')


class SshExecutor(Executor, ABC):
    def __init__(self, job, downscope=False):
        Executor.__init__(self, job, downscope=downscope)

    def run_direct(self, params="", wrapper_add="", script=""):
        self.execute(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                allocator='ssh',
                                params=params,
                                target_script=script,
                                wrapper_add=wrapper_add, main_script=True)
        return ''

    def management_handler(self, helper_script="", params="", wrapper_add="", script="", install_env=False):
        if helper_script == '':
            helper_script = f"{self.job.scripts_path}/runHelper.sh"

        management_out = self.execute(helper_script=helper_script,
                                      allocator='ssh',
                                      params=params,
                                      wrapper_add=wrapper_add,
                                      pre_exec_scripts=[f'{self.job.scripts_path}/ssh.env'],
                                      target_script=script, install_env=install_env,
                                      text=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout
        logging.debug(management_out)
        return management_out
