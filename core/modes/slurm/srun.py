from core.modes.common import *
from core.modes.base import ModeBase
from core.utility.executor import Slurm_Executor


class Slurm(ModeBase, ABC):
    considered_env_prefixes = ['SLURM', 'SRUN', 'SALLOC']
    executor = None

    def custom_prepare(self):
        # install gitlab runner if necessary
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                         wrapper_add=f'/usr/bin/mkdir -p {self.job.user_path}')

        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         wrapper_add=f'/usr/bin/cp /dev/stdin '
                                                     f'{self.job.user_path}/wrapper{self.job.jobid}.sh',
                                         script=f"{self.job.scripts_path}/shellWrapper.sh")

        git_runner_command = [f'{self.job.shell_path} -l',
                              f"{self.job.user_path}/wrapper{self.job.jobid}.sh"]
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         wrapper_add=' '.join(git_runner_command),
                                         script=f"{self.job.scripts_path}/runnerInstallHelper.sh",
                                         install_env=True)

    @staticmethod
    def env_copy():
        user = os.getenv("USER")
        env = {k: v for k, v in os.environ.items() if not v.__contains__(user) or k == "PATH"}
        export_env = ""
        for k, v in env.items():
            export_env = f'{export_env}{k}="{v}",'
        return export_env[:-1]

    def get_combiner_script(self):
        return f"{self.job.scripts_path}/xPipeHelper.sh"

    def __init__(self, job):
        ModeBase.__init__(self, job)
        self.slurm_simple_job_id = None
        self.executor = Slurm_Executor(job, job.down_scoping)

    def get_run_parameters(self):
        return f'--job-name=CI_{self.job.jobid} ' + self.job.get_parameters()

    def get_run_wrapper(self):
        return f' {self.job.shell_path} -l'

    def get_run_script(self):
        return self.job.exec_script

    def get_simple_run_wrapper(self):
        return f"{self.job.shell_path} -l {self.job.user_path}/wrapper{self.job.jobid}.sh"

    def run_main_script(self):
        out = self.executor.run_direct(params=self.get_run_parameters(), wrapper_add=self.get_run_wrapper(),
                                 script=self.get_run_script())
        print(out)

    def run_simple_script(self):
        out = self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         params=self.get_simple_run_parameters(),
                                         wrapper_add=self.get_simple_run_wrapper(),
                                         script=self.get_simple_run_script())
        print(out)

    def cleanup(self):
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                         wrapper_add=f'/usr/bin/rm {self.job.user_path}/wrapper{self.job.jobid}.sh')
        ModeBase.cleanup(self)
        self.executor.cancel()
