from core.modes.common import *
from .srun import Slurm
from .batch import Default, Sbatch
from core.modes.base import ModeBase


class Singularity(Slurm, ABC):
    container = ''

    def get_run_parameters(self):
        parameters = Slurm.get_run_parameters(self)
        # get container for singularity
        self.container = get_cenv('CONTAINER')
        if self.container is None:
            ModeBase.abort(self, "Error: No container defined, use variable CONTAINER")
        if os.path.exists(self.container):
            self.container = f'{self.job.clone_path}/{self.container}'
        # add singularity specific parameter
        return parameters + f' --export=CONTAINER={self.container}'

    def get_run_wrapper(self):
        # Generation of the singularity script within user space
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/xPipeHelper.sh",
                                         wrapper_add=f'/usr/bin/cp /dev/stdin '
                                                     f'{self.job.user_path}/script{self.job.jobid}.sh',
                                         script=self.get_singularity_script())
        return Slurm.get_run_wrapper(self) + f" {self.job.user_path}/script{self.job.jobid}.sh"

    def cleanup(self):
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                         wrapper_add=f'/usr/bin/rm {self.job.user_path}/wrapper{self.job.jobid}.sh')
        ModeBase.cleanup(self)
        self.executor.management_handler(helper_script=f"{self.job.scripts_path}/runHelper.sh",
                                         wrapper_add=f'/usr/bin/rm {self.job.user_path}/script{self.job.jobid}.sh')
        self.executor.cancel(f'{ModeBase.get_jobid_from_file(self.slurm_jobid_file)}')

    def get_singularity_script(self):
        script = ""
        # get container for singularity
        self.container = get_cenv('CONTAINER')
        if self.container is None:
            ModeBase.abort(self, "Error: No container defined, use variable CONTAINER")
        if os.path.exists(self.container):
            script += f'{self.job.scripts_path}/singularityLocalRunstep.sh'
        else:
            script += f'{self.job.scripts_path}/singularityRunstep.sh'
        return script


