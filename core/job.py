import json
import os
import logging
import sys
import time
import importlib.resources as importlib_resources

import core.modes as modes
from core.utility import get_cenv, defines
import core.utility as utility
import core.authentication.JWTManager as jwt
import core.authentication.JSONManager as man
from filelock import Timeout, FileLock


class Job:
    user_path = 'Runner'
    key_path = None
    runner_path = None
    map_path = None
    aes_path = None
    down_scoping = True
    jobid = None
    pipeline_id = None
    account = None
    exec_script = ''
    allow_failure = False
    shared_tmp = ''
    tmp_dir = ''
    stage_tmp_dir = ''
    error_code_file = ''
    errcode_dict = dict()
    mode_name = ''

    @property
    def mode(self):
        return self._mode

    @property
    def custom_env(self):
        if self.mode:
            return self.mode.custom_env
        return dict()

    @property
    def build_path(self):
        return f'{self.user_path}/cache/{get_cenv("CI_CONCURRENT_PROJECT_ID")}/{get_cenv("CI_PROJECT_PATH_SLUG")}'

    @property
    def cache_path(self):
        return f'{self.user_path}/cache/{get_cenv("CI_CONCURRENT_PROJECT_ID")}/{get_cenv("CI_PROJECT_PATH_SLUG")}'

    @property
    def clone_path(self):
        return get_cenv('CI_PROJECT_DIR')

    def __get_config(self):
        if defines.debug:
            try:
                import pdb_attach
                import random
                random.seed(int(get_cenv("CI_JOB_ID")))
                port = random.randint(1024, 65535)
                pdb_attach.listen(port)
                if self.args[1] != 'config':
                    logging.debug(f'Debug mode on listening on port {port}')
                    time.sleep(100)
            except ImportError:
                if self.args[1] != 'config':
                    logging.warning('Debug mode on but pdb_attach module not found')

        def rep(s):
            return os.path.expanduser(os.path.expandvars(s))

        try:
            settings = json.loads(importlib_resources.read_text('settings', 'config.json'))
        except:
            config_dir = os.getenv("XDG_CONFIG_HOME", f"{os.getenv('HOME')}/.config") + '/ci_driver'
            with open(f'{config_dir}/config.json') as config_fp:
                settings = json.load(config_fp)

        self.down_scoping = (settings["Downscope"] == 'True' or settings["Downscope"] == 'true')
        self.key_path = rep(settings["Key Path"])
        self.map_path = rep(settings["Map Path"])
        self.runner_path = rep(settings["Runner Path"])
        self.aes_path = rep(settings["AES Path"])
        self.shell_path = rep(settings["Shell Install"])
        self.log_path = rep(settings["Log Path"])
        self.shell_config = rep(settings["Shell Config"])
        self.rt_utility_path = rep(settings["Runtime Utility Path"])
        self.tmp_var = rep(settings["TMP Variable"])
        self.scripts_path = f'{self.rt_utility_path}/scripts'
        if self.down_scoping:
            try:
                uid, pid = jwt.get_UID_PID_ID_TOKEN(get_cenv('HPC_CI_TOKEN'),
                                                    f"{get_cenv('CI_SERVER_URL')}/oauth/discovery/keys")
            except Exception as e:
                logging.error("id token HPC_CI_TOKEN, with aud: aixCIlence not defined. "
                        "More info at: https://git.rwth-aachen.de/adrianschmitz2/aixcilenz-ci-driver/-/wikis/Modes-and-Variables \n"
                        f"Exception: {e} \n"
                        f"HPC_CI_TOKEN: {get_cenv('HPC_CI_TOKEN')} \n"
                        f"CI_SERVER_URL: {get_cenv('CI_SERVER_URL')} \n")
                exit(1)

            self.account = man.get_account(get_cenv('CI_SERVER_URL'), pid, uid, self.key_path, self.map_path,
                                           self.aes_path)
            self.user_path = settings["User Path"].replace("$USER", self.account)
            if self.account is None:
                logging.error(
                    f"No mapping for GitLab project: {get_cenv('CI_PROJECT_NAME')}, or GitLab user: "
                    f"{get_cenv('GITLAB_USER_NAME')} available. Please register CI support for to access the Runner")
        else:
            self.user_path = rep(settings['User Path'])

    def __setup(self):
        self.__get_config()
        if not self.down_scoping:
            self.account = os.getenv("USER")
        self.jobid = get_cenv('CI_JOB_ID')
        self.pipeline_id = get_cenv('CI_PIPELINE_ID')
        self.allow_failure = get_cenv('ALLOW_FAILURE', False)
        self.driver_cache = os.getenv('XDG_CACHE_HOME', f'{os.getenv("HOME")}/.cache') + '/ci_driver'
        os.makedirs(self.driver_cache, exist_ok=True)
        self.shared_tmp = f'{self.driver_cache}/shared_tmp/{self.pipeline_id}'
        self.concurrent_tmp = f'{self.driver_cache}/{get_cenv("CI_RUNNER_ID")}/concurrent_tmp/{get_cenv("CI_CONCURRENT_ID")}'
        #os.makedirs(self.concurrent_tmp, exist_ok=True)
        self.tmp_dir = f'{self.concurrent_tmp}/{self.pipeline_id}'
        self.stage_tmp_dir = f'{self.tmp_dir}/stages/{get_cenv("CI_JOB_STAGE")}'
        self.setup_logging()

    def setup_logging(self):
        try:
            pipeline_creation_day_utc = get_cenv('CI_PIPELINE_CREATED_AT').split('T')[0]
        except:
            print('No pipline creation day found')
            pipeline_creation_day_utc = get_cenv('CI_PIPELINE_CREATED_AT')
        self.log_path = f'{self.log_path}/{self.account}/{pipeline_creation_day_utc}/{self.pipeline_id}'
        os.makedirs(self.log_path, exist_ok=True)
        logging.basicConfig(filename=f'{self.log_path}/{self.jobid}.log',
                            format=f'[%(asctime)s] [%(filename)s:%(lineno)s:%(funcName)s] %(levelname)s:%(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S',
                            encoding='utf-8',
                            level=defines.log_level)
        logging.getLogger('filelock').setLevel(logging.INFO)
        if not get_cenv('CI_LOG_STDOUT', 'False') in ['0', 'NULL', 'False', 'false',
                                                      'FALSE'] and defines.stdout_logging and self.args[1] != 'config':
            logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    def __init__(self, args):
        self.args = args
        self.mode_name = get_cenv('CI_MODE', 'Slurm').strip()
        self._mode = None
        self.__setup()
        if not self.allow_failure and self.args[1] == 'run':
            self.error_code_file = f'{self.stage_tmp_dir}/{self.args[3]}.json'
            os.makedirs(self.stage_tmp_dir, exist_ok=True)
        logging.info(f'Started step {self.args[1]} with args={self.args}')

        self._mode = modes.get_mode(self)
        if self.args[1] == 'config':
            self.config()
        elif self.args[1] == 'prepare':
            self.prepare()
        elif self.args[1] == 'run':
            self.run()
        elif self.args[1] == 'cleanup':
            self.cleanup()
        else:
            logging.error(f'Unknown driver event: {self.args[1]}')
        logging.info(f'Finished step {self.args[1]}')

    def prepare(self):
        # prepare directories
        if os.path.isdir(self.concurrent_tmp):
            now = time.time()
            for p in os.listdir(f'{self.concurrent_tmp}'):
                if (p != self.pipeline_id and os.path.isdir(f'{self.concurrent_tmp}/{p}') and
                    os.stat(f'{self.concurrent_tmp}/{p}').st_mtime < (now - defines.cc_tmp_del_range * defines.seconds_per_day)):
                    logging.debug(f'Removing concurrent_tmp: {self.concurrent_tmp}/{p}')
                    os.system(f'rm -r {self.concurrent_tmp}/{p}')

        self._mode.custom_prepare()

    def config(self):
        # Allocate the Slurm job here already to get set build dir to Slurm's tmp
        _build_path = self.build_path
        _cache_path = self.cache_path
        _builds_dir_is_shared = True
        if get_cenv('CI_MODE') == 'SingleSlurmJobAcrossStages':
            self._mode.get_custom_config()
            _build_path = f'{self._mode.tmp_dir}/builds'
            _cache_path = f'{self._mode.tmp_dir}/cache'
            _builds_dir_is_shared = False
        builder = {'builds_dir': _build_path,
                   'cache_dir': _cache_path,
                   'builds_dir_is_shared': _builds_dir_is_shared,
                   'hostname': 'custom-hostname',
                   'driver': {'name': defines.name, 'version': defines.version},
                   'job_env': {'GIT_DEPTH': '1'}}

        print(json.dumps(builder))
        logging.debug(builder)

        pipeline_ci_variables = ["CI_PROJECT_ID", "CI_PROJECT_PATH", "GITLAB_USER_NAME",
                                 "GITLAB_USER_ID", "CI_PIPELINE_ID", "CI_PIPELINE_URL",
                                 "CI_JOB_STARTED_AT", "CI_PIPELINE_SOURCE", "CI_COMMIT_SHA",
                                 "CI_COMMIT_AUTHOR", "GITLAB_USER_EMAIL", "CI_COMMIT_BRANCH"]
        pipeline_info = {k: get_cenv(k) for k in pipeline_ci_variables}
        pipeline_info['cluser_account'] = self.account
        pipeline_info_path = f'{self.log_path}/pipeline_info.json'
        if not os.path.isfile(pipeline_info_path):
            with open(pipeline_info_path, 'w+') as pipeline_info_fp:
                json.dump(pipeline_info, pipeline_info_fp, indent=2)

        logging.info(f'ci_mode: {get_cenv("CI_MODE")}')
        logging.info(f'ci_job_id: {get_cenv("CI_JOB_ID")}')

    def get_parameters(self, prefixes=['SLURM_PARAM']):
        parameter_string = ''
        # NOTE: Preserve order of prefixes, so that later prefixes overwrite prior ones
        for prefix in prefixes:
            parameter_string += ' '.join([v for k,v in os.environ.items() if k.startswith(f'CUSTOM_ENV_{prefix}')])
        return parameter_string

    def run(self):
        if not self.allow_failure:
            utility.update_json_kv(self.error_code_file, self.jobid, -1)
        self.exec_script = self.args[2]
        if self.args[3] not in ['build_script', 'step_script']:
            self._mode.run_simple_script()
        else:
            self._mode.run_main_script()
            self._mode.run_post_script()

    def cleanup(self):
        self._mode = modes.get_mode(self)
        logging.debug(f'attempting clone deletion {get_cenv("AIX_CI_CLEANUP")}')
        if get_cenv("AIX_CI_CLEANUP") is not None:
            logging.debug(f'doing cleanup deleting clone')
            self._mode.executor.management_handler(helper_script=f"{self.scripts_path}/runHelper.sh",
                                                 wrapper_add=f"/usr/bin/rm -rf {get_cenv('CI_PROJECT_DIR')}")
            logging.debug(f'doing cleanup deleted clone')
        self._mode.cleanup()
