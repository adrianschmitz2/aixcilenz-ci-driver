import json
import core.authentication.EncryptionManager as encrypt
from filelock import FileLock
import logging
import sys


def add_id_mapping(path, CI_id, slurm_id):
    lock = FileLock(f"{path}.lock")
    with lock:
        try:
            with open(path, "r") as file:
                mapping = json.loads(file.read())
        except IOError:
            mapping = {}
        mapping[CI_id] = slurm_id
        new_mapping = json.dumps(mapping)
        with open(path, "w") as file:
            file.write(new_mapping)


def remove_id_mapping(path, CI_id):
    lock = FileLock(f"{path}.lock")
    with lock:
        with open(path, "r") as file:
            mapping = json.loads(file.read())
        try:
            del mapping[CI_id]
        except (KeyError):
            logging.warning(f'Tried to remove internal Slurm ID from mapping')
        new_mapping = json.dumps(mapping)
        with open(path, "w") as file:
            file.write(new_mapping)


def get_account(url, pid, uid, key_path, map_path, aes_path):
    search_file = encrypt.read_mapping(key_path, map_path, aes_path)
    instance = search_file[url]
    result = None
    try:
        result = instance["uid"][uid]["acc"]
    except KeyError:
        try:
            result = instance["pid"][pid]["acc"]
        except KeyError:
            logging.error("Cannot assign GitLab user/project to cluster account. Please register here: TODO")
            sys.exit(1)
    return result
