import jwt
import time


def get_UID_PID_ID_TOKEN(JWT, url):
    jwks_client = jwt.PyJWKClient(url)
    signing_key = jwks_client.get_signing_key_from_jwt(JWT)
    # wait for token to be valid
    time.sleep(2)
    data = jwt.decode(JWT,
                      signing_key.key,
                      algorithms=["RS256"],
                      audience="aixCIlenz",
                      options={"verify_exp": False})
    return data["user_id"], data["project_id"]