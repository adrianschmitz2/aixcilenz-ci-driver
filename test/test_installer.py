import unittest

import os
import tempfile
import subprocess

class TestInstaller(unittest.TestCase):


    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.install_path =f'--install-path={self.test_dir}'
        subprocess.run(['python','Installer.py', f'{self.install_path}', 'i', '-d'])
        
class TestInstaller(TestInstaller):
    
    def test_create_keys(self):
        files = ('/id_rsa', '/id_rsa.pub', '/Keyfile', '/Assignments.txt')
        for file in files:
            path = self.test_dir + file
            self.assertTrue(os.path.isfile(path))

    #test_driver_config
        path = self.test_dir + '/settings/config.json'
        self.assertTrue(os.path.isfile(path))

        self.assertTrue(os.path.isfile('utility/install/requirements-static.txt'))
        self.assertTrue(os.path.isfile('utility/install/build_static_driver.sh'))



if __name__ == "__main__":
    unittest.main()