import os
import sys
import logging
import subprocess
import unittest
from core.job import Job
from core.modes import *
from core.utility.executor import Executor, Slurm_Executor, SshExecutor

os.environ["CUSTOM_ENV_CI_PIPELINE_CREATED_AT"]= '01032024'
global cwd 
cwd = os.getcwd()
global home
home = os.getenv("HOME")
global tmp 
tmp= os.getenv("TMP")
global job_prepare_args, job_run_args, job_cleanup_args, job_config_args
job_prepare_args= [f'{cwd}/install/driver.py', 'prepare']
job_run_args =[f'{cwd}/install/driver.py', 'run', '','prepare_script']
job_cleanup_args =[f'{cwd}/install/driver.py', 'cleanup']
job_config_args = [f'{cwd}/install/driver.py', 'config']

class TestInstaller(unittest.TestCase):
    def test_job_execution(self):
        isconfig = os.path.isfile('install/settings/config.json')
        if isconfig:
            print('config.json file exists')
        else:
            print('config.json file does not exist')

        job = Job(job_config_args)
        user_path = job.user_path

        command_cp = f"INFO:root:Executing command: ['{cwd}/install/utility/runtime/scripts/xPipeHelper.sh', '   /usr/bin/cp /dev/stdin {user_path}/wrapperNone.sh', '{cwd}/install/utility/runtime/scripts/shellWrapper.sh']"
        command_helper = f"INFO:root:Executing command: ['{cwd}/install/utility/runtime/scripts/xPipeHelper.sh', '   --export=CUSTOM_SHELL_CONFIG=.zshrc /usr/bin/env bash -l {user_path}/wrapperNone.sh', '{cwd}/install/utility/runtime/scripts/runnerInstallHelper.sh']"

        with self.assertLogs( level=logging.INFO) as lg:
            Job(job_prepare_args)
            
            # print(lg.output)
            # print("-----------------------------------------------------------")
            # print(command_cp)

            #test if the command_mkdir works
            self.assertTrue(os.path.isdir(f'{home}/.cache/ci_driver'))
            self.assertTrue(command_cp in lg.output)
            self.assertTrue(command_helper in lg.output)
        Job(job_run_args)
    
        command_rm = f"INFO:root:Executing command: ['{cwd}/install/utility/runtime/scripts/runHelper.sh', '   /usr/bin/rm {user_path}/wrapperNone.sh', '']"
      
        with self.assertLogs( level=logging.INFO) as lg:
            Job(job_cleanup_args)
            self.assertTrue(command_rm in lg.output)
            self.assertFalse(os.path.isfile(f'{home}/.cache/ci_driver/wrapperNone.sh'))
            

if __name__ == '__main__':
    unittest.main()
