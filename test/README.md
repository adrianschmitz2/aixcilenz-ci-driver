# CustomDriver Tesing

## Current Testing Status

# Project tree

.

 * [core](./core)
    * [test_jsonManager.py](./core/test_jsonManager.py ) - [x]
    * [test_jwtManager.py](./core/test_jwtManager.py) - [x]
    * [job.py](./core/job.py) - [x]
 * [modes](./modes)
    * [test_executor.py](./modes/test_executor.py) - [x]
    * [test_installer.py](./modes/test_installer.py) - [x]
 * [account_manager](./account_manager)
    * [test_manager.py](./account_manager/test_manager.py) - [x]
