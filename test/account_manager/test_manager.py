import shutil
import tempfile
import os

import unittest
import AccountManager as manager


class TestAccManager(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.global_args = [f'--base-path={self.test_dir}']
        manager._setup_logging('DEBUG', f'{self.test_dir}/history.log', std=False)

    def _run(self, command, command_opts=[]):
        self.args = manager.CLI( [command]+ command_opts +self.global_args)
        self.args.func(self.args)

    def _init(self, url, accname):
        
        self._run('init', [f'--gitlab-url={url}',f'--cluster-account-name={accname}'])

    def _add_id(self, id, url, accname, id_type='uid'):
        s_type = 'user' if id_type=='uid' else 'project'
        self._run('add', [f'--gitlab-{s_type}-id={id}', f'--gitlab-url={url}', f'--cluster-account-name={accname}'])

    def _rm_id(self,url, id, accname, id_type='uid'):
        s_type = 'user' if id_type=='uid' else 'project'
        self._run('remove', [ f'--gitlab-url={url}',f'--gitlab-{s_type}-id={id}',  f'--cluster-account-name={accname}'])

    def _rm_url(self, url):
        self._run('remove', [f'--gitlab-url={url}'])

    def tearDown(self):
#        with open(f'{self.test_dir}/history.log', 'r') as history:
#            print(history.readlines())
        shutil.rmtree(self.test_dir)


class TestInit(TestAccManager):
    def test_init(self, url='gitlab.com', accname='john'):
        self._init(url, accname)
        files = (self.args.priv_key_file, self.args.pub_key_file,
                 self.args.mapping_file, self.args.aes_key_file)
        for file in files:
            self.assertTrue(os.path.isfile(file))


class TestAdd(TestAccManager):
    def _test_add_id(self, id='0', url='gitlab.com', accname='john', id_type='uid'):
        self._init(url, accname)
        self._add_id(id, url, accname, id_type)
        self.assertTrue(manager._get_mapping(self.args)[url][id_type][id]['acc'] == accname)

    def test_add_uid(self):
        self._test_add_id()


    def test_add_pid(self):
        self._test_add_id(id_type='pid')


class TestRemove(TestAccManager):
    def _test_rm_id(self, url='gitlab.com', id='0', accname='john', id_type='uid'):
        self._init(url, accname)
        self._add_id(id, url, accname, id_type)
        self.assertTrue(manager._get_mapping(self.args)[url][id_type][id]['acc'] == accname)
        self._rm_id(url, id,accname, id_type)
        self.assertFalse(manager._id_present(self.args, id, id_type))

    def test_remove_uid(self):

        self._test_rm_id(id_type='uid')

    def test_remove_pid(self):
        self._test_rm_id(id_type='pid')


if __name__ == '__main__':
    unittest.main()
