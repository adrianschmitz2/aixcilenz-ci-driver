import unittest
import json
from unittest.mock import patch, mock_open, call

import core.authentication.JSONManager as man


class TestInstaller(unittest.TestCase):

    path = 'test_mapping.json'
    CI_id = 'CI_ID_123'
    slurm_id = 'SLURM_ID_456'

    @patch('builtins.open', new_callable=mock_open, read_data = '{}')

    def test_add_id_mapping(self, mock_file):


        man.add_id_mapping(self.path, self.CI_id, self.slurm_id)

        mock_file.assert_called_with(self.path, 'w')
        calls = [call(self.path, 'r'), call().read()]
        mock_file.assert_has_calls(calls, any_order=True)

        expected_mapping = {self.CI_id: self.slurm_id}
        mock_file().write.assert_called_once_with(json.dumps(expected_mapping))

    @patch('builtins.open', new_callable=mock_open, read_data = '{}')

    def test_remove_id_mapping(self, mock_file):

        man.remove_id_mapping(self.path, self.CI_id)

        mock_file.assert_called_with(self.path, 'w')
        calls = [call(self.path, 'r'), call().read()]
        mock_file.assert_has_calls(calls, any_order=True)
        expected_mapping = {}
        mock_file().write.assert_called_once_with(json.dumps(expected_mapping))
    @patch('core.authentication.EncryptionManager.read_mapping')
    def test_get_account(self, mock_read_mapping):
    
        url = 'https://mock-example.com'
        pid = 'pid'
        uid = 'uid'
        key_path = 'key_path'
        map_path = 'map_path'
        aes_path = 'aes_path'
        mock_read_mapping.return_value = {'https://mock-example.com': {'uid': {'uid': {'acc': 'account'}}}}
        result = man.get_account(url, pid, uid, key_path, map_path, aes_path)
        self.assertEqual(result, 'account')


if __name__ == "__main__":
    unittest.main()