import unittest
import tempfile
import os
import subprocess
import json
from unittest import mock
from unittest.mock import MagicMock, patch, mock_open
from core.job import Job
import Installer

def generate_test_driver_config(dest_path):
    
    os.makedirs(os.path.dirname(f'{dest_path}/config.json'), exist_ok = True)
    #try to copy the config.json in settings into dest_path
    subprocess.run(['cp', 'install/settings/config.json', f'{dest_path}/config.json' ])
    with open(f'{dest_path}/config.json', "r") as fp_config:
        data = json.load(fp_config)
    data["Downscope"] = "False"
    with open(f'{dest_path}/config.json', "w") as fp_config:
        json.dump(data, fp_config, indent=4)


class TestJob(unittest.TestCase):
    #create a test config json file
    test_config_dir = os.getenv("XDG_CONFIG_HOME", f"{os.getenv('HOME')}/.config") + '/ci_driver'
    generate_test_driver_config(test_config_dir)


    def test_get_config(self):
        job_instance = Job([self, 'config'])
        self.assertTrue("/install/id_rsa" in job_instance.key_path)
        self.assertTrue("/install/Assignments.txt" in job_instance.map_path)
        self.assertTrue("/install" in job_instance.runner_path)
        self.assertTrue("Keyfile" in job_instance.aes_path) 
        self.assertTrue("/install/logs" in job_instance.log_path)
        self.assertTrue("install/utility/runtime" in job_instance.rt_utility_path) 
        self.assertEqual(job_instance.account, os.getenv('USER'))
        


    @patch('core.modes.get_mode')
    def test_cleanup(self, mock_get_mode):
        job_instance = Job([self,'cleanup'])
        mock_get_mode.assert_called()
     


if __name__ == '__main__':
    unittest.main()