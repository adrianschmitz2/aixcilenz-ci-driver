import unittest
import json
from unittest.mock import patch,MagicMock

import core.authentication.JWTManager as jwt_man


class TestJWTFunctions(unittest.TestCase):


    @patch("jwt.PyJWKClient")
    @patch("time.sleep")
    @patch("jwt.decode")


    def test_get_UID_PID_ID_TOKEN(self, mock_jwt_decode, mock_sleep, mock_jwk_client):
        mock_jwt = "mock_jwt"
        mock_url = "https://git-ce.rwth-aachen.de"
        mock_data = {"user_id": "mock_user_id", "project_id": "mock_project_id"}
        # Configure mock behavior
        mock_jwk_instance = MagicMock()
        mock_jwk_instance.key ='mock_key'
        mock_jwk_client.return_value.get_signing_key_from_jwt.return_value = mock_jwk_instance
        mock_sleep.return_value = None
        mock_jwt_decode.return_value = mock_data
        # Call the function
        result = jwt_man.get_UID_PID_ID_TOKEN(mock_jwt, mock_url)

        # Assertions
        self.assertEqual(result, ("mock_user_id", "mock_project_id"))
        mock_jwk_client.assert_called_once_with(mock_url)
        mock_jwt_decode.assert_called_once_with(
            mock_jwt,
            mock_jwk_instance.key,
            algorithms=["RS256"],
            audience="aixCIlenz",
            options={"verify_exp": False}
        )
        mock_sleep.assert_called_once_with(2)

if __name__ == '__main__':
    unittest.main()
