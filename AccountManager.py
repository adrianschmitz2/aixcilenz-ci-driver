#!/usr/bin/env python3
import argparse
import string
import random
import os
import sys
import logging
from pprint import pprint

import core.authentication.EncryptionManager as encrypt
import core.utility.cli as cli

# Define Commandline interface
def CLI(_args):
    mod_common_pars = [
        {
            "flags": ["-uid", "--gitlab-user-id"],
            "type": str,
            "metavar": "<uid>",
            "help": "The gitlab uid of the assigned user."
        },
        {
            "flags": ["-pid", "--gitlab-project-id"],
            "type": str,
            "metavar": "<pid>",
            "help": "The gitlab pid of the assigned project."
        },
        {
            "flags": ['-url', '--gitlab-url'],
            "type": str,
            "metavar": "<pub>",
            "required": True,
            "help": "URL of the targeted Gitlab instance."
        },        {
            "flags": ['-pub', '--pub-key-file'],
            "type": str,
            "metavar": "<file>",
            "default": "key.pub",
            "help": "Path to the public key file.",
        },        {
            "flags": ["-acc", "--cluster-account-name"],
            "type": str,
            "metavar": "<acc>",
            "required": True,
            "help": "Name of the assigned cluster account."
        },{
            "flags": ["-priv", "--priv-key-file"],
            "type": str,
            "metavar": "<file>",
            "default": "key",
            "help": "Path to the private key file."
        },
        {
            "flags": ["-aes", "--aes-key-file"],
            "type": str,
            "metavar": "<file>",
            "default": "Keyfile",
            "help": "Path to the AES key."
        },
        {
            "flags": ["-map", "--mapping-file"],
            "type": str,
            "metavar": "<file>",
            "default": "Assignments.txt",
            "help": "Path to the account mapping file."
        },
        {
            "flags": ["-bp", "--base-path"],
            "type": str,
            "metavar ": "<path>",
            "default": "./",
            "help": "Interpret keys, mapping etc. relative to this path. Default: cwd"
        },
        {
            "flags": ["-lf", "--log-file"],
            "type": str,
            "metavar": "<file>",
            "default": "history.log",
            "help": "Log history in this file. Default: history.log"
        },
        {
            "flags": ["-ll", "--log-level"],
            "type": str,
            "metavar": "<DEBUG|INFO|WARNING|ERROR|CRITICAL>",
            "default": "INFO",
                "help": "Log level. Default: INFO"
        },


    ]

    subcommands = {
        'init': {
            'aliases': ['i'],
            'f': init_mapping
        },
        'remove': {
            'aliases': ['rm'],
            'f': remove
        },
        'add': {
            'aliases': ['a'],
            'f': add
        },
        'print': {
            'aliases': ['p'],
            'f': print_mapping
        },
    }

    parameters = {
    'init': [
        *mod_common_pars,
    ],
    'remove': [        
        *mod_common_pars,
    ],
    'add': [
        {
            "flags": ["-del", "--delete-date"],
            "type": str,
            "metavar": "<del>",
            "default": "never",
            "help": "The deletion date of the coupling. Default: never"
        },
        *mod_common_pars,
    ],
    'print': [],
    'global': [
    ]
    }

    parser = argparse.ArgumentParser(prog='Aixcellenz CI AccountManager')
    cli.add_parameters(parser, parameters['global'])
    subparsers = parser.add_subparsers(help='sub-command help')

    for cmd in subcommands:
        subcmd_parser = subparsers.add_parser(cmd, help=f'{cmd} help', aliases=subcommands[cmd]['aliases'])
        subcmd_parser.set_defaults(func=subcommands[cmd]['f'])
        cli.add_parameters(subcmd_parser, parameters[cmd])

    ret = parser.parse_args(_args)
    ret.base_path = os.path.abspath(os.path.expandvars(os.path.expanduser(ret.base_path)))
    os.makedirs(ret.base_path, exist_ok=True)
    args_to_expand = ['mapping_file', 'priv_key_file', 'pub_key_file', 'aes_key_file',
                      'log_file']
    for arg in args_to_expand:
        if arg in ret:
            tmp = os.path.expandvars(os.path.expanduser(vars(ret)[arg]))
            if not os.path.isabs(tmp):
                vars(ret)[arg] = os.path.join(ret.base_path, tmp)
    return ret


def get_random_string(length: int) -> str:
    # choose from all lowercase letter
    letters = string.ascii_letters + string.digits
    result_str = ''.join(random.choice(letters) for _ in range(length))
    return result_str

def _init_mapping(priv_key_file, pub_key_file, mapping_file, aes_key_file):
    if not (os.path.isfile(priv_key_file) and os.path.isfile(pub_key_file)):
        encrypt.create_keys(priv_key_file, pub_key_file)
    with open(mapping_file, "w") as text_file:
        text_file.write('')
    if not os.path.isfile(aes_key_file):
        encrypt.set_AES_key(get_random_string(245), aes_key_file, pub_key_file)
    logger.info(f'Added initial mapping at {mapping_file} with aes_key at {aes_key_file}')

def init_mapping(args):
    #pprint(args)
    if os.path.isfile(args.mapping_file):
        logger.error(f'Mapping at {args.mapping_file} already exists. (aborting)')
        sys.exit(1)
    _init_mapping(args.priv_key_file, args.pub_key_file,
                  args.mapping_file, args.aes_key_file)


def _add_id(url, id, priv_key_file, mapping_file, cluster_acc, delete_date, aes_key_file, id_type='uid'):
    mapping = encrypt.read_mapping(priv_key_file, mapping_file, aes_key_file)
    if url not in mapping:
        mapping[url] = {"uid": {}, "pid": {}}
    if id in mapping[url][id_type]:
        logger.error(f"Mapping for project={id} at gitlab instance={url} already present (aborting)")
        sys.exit(1)
    id_dict = {"acc": cluster_acc, "delete": delete_date}
    mapping[url][id_type][id] = id_dict
    encrypt.write_mapping(mapping, priv_key_file, mapping_file, aes_key_file)
    logger.info(f'Added CI access for url={url}, id_type={id_type}, id={id}, acc={cluster_acc}, delete={delete_date} at mapping={mapping_file}')

def add(args):
    if args.gitlab_user_id:
        _add_id(args.gitlab_url, args.gitlab_user_id, args.priv_key_file, args.mapping_file,
                args.cluster_account_name, args.delete_date, args.aes_key_file, 'uid')
    if args.gitlab_project_id:
        _add_id(args.gitlab_url, args.gitlab_project_id, args.priv_key_file, args.mapping_file,
                args.cluster_account_name, args.delete_date, args.aes_key_file, 'pid')
    if not args.gitlab_user_id and not args.gitlab_project_id:
        logger.debug(f'Could not add due to missing user or project id')


def _remove_id(url, id, priv_key_file, mapping_file, aes_key_file, id_type='uid'):
    try:
        mapping = encrypt.read_mapping(priv_key_file, mapping_file, aes_key_file)
        cluster_account = mapping[url][id_type][id]["acc"]
        del mapping[url][id_type][id]
        encrypt.write_mapping(mapping, priv_key_file, mapping_file, aes_key_file)
        logger.info(f"Removed CI access for cluster account={cluster_account}")
    except KeyError as e:
        logger.error(f'Could not remove id={id}({id_type}) from mapping={mapping_file}. (aborting)')
        sys.exit(1)

def _remove_url(url, priv_key_file, mapping_file, aes_key_file):
    try:
        mapping = encrypt.read_mapping(priv_key_file, mapping_file, aes_key_file)
        del mapping[url]
        encrypt.write_mapping(mapping, priv_key_file, mapping_file, aes_key_file)
        logger.info(f"Removed CI access for url={url}")
    except KeyError as e:
        logger.error(f'Could not remove url={url} from mapping={mapping_file} (aborting)')
        sys.exit(1)

def remove(args):
    if args.gitlab_user_id:
        _remove_id(args.gitlab_url, args.gitlab_user_id, args.priv_key_file, args.mapping_file,
                   args.aes_key_file, 'uid')
    if args.gitlab_project_id:
        _remove_id(args.gitlab_url, args.gitlab_project_id, args.priv_key_file, args.mapping_file,
                   args.aes_key_file, 'pid')
    if not args.gitlab_user_id and not args.gitlab_project_id:
        _remove_url(args.gitlab_url, args.priv_key_file, args.mapping_file, args.aes_key_file)

def _get_mapping(args):
    return encrypt.read_mapping(args.priv_key_file, args.mapping_file, args.aes_key_file)

def _id_present(args, id, id_type='uid'):
    try:
        encrypt.read_mapping(args.priv_key_file, args.mapping_file, args.aes_key_file)[args.gitlab_url][id_type][id]
        return True
    except:
        return False


def print_mapping(args):
    pprint(_get_mapping(args))

def _setup_logging(level, filename, std=True):
    global logger
    logger = logging.getLogger(__file__)
    logger.setLevel(level)
    file_handler = logging.FileHandler(filename, encoding='utf-8')

    formatter = logging.Formatter('[%(asctime)s] %(levelname)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(level)
    if std:
        std_handler = logging.StreamHandler()
        std_handler.setLevel(level)
        logger.addHandler(std_handler)
    logger.addHandler(file_handler)


if __name__ == '__main__':
    args = CLI(sys.argv[1:])
    _setup_logging(getattr(logging, args.log_level.upper(), 20), args.log_file)
    args.func(args)
