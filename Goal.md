# Targets

## Integration Support

* Slurm integration
* Jube support
* Singularity support
* (Shell runner)

### Slurm Integration

#### Default (Single node)

* A job allocates a node via Slurm
* Store Job id
* Copy tmp scripts to $HOME/"script"
* Once the node is provided, access the node interactively via ssh
* Run scripts on interactive node
* Cleanup cancels job
* ssh shell stdin and stdout are piped to opened shell

#### Job submission (Benchmarking/Multi node)

* Job script available in GitLab
* Submits Job to queue and waits
* Save job id 
* If possible, define output as artifact
* Cleanup cancels job

#### Variables

* Direct submission
* Slurm parameters

### Singularity

* Submit a slurm job
* Save job id
* Run Container on each node
* Run Script within container
* Cleanup cancels job

### JUBE

* Ensure a cluster wide JUBE installation via (local) module
* A job allocates a node via Slurm
* Store Job id
* Copy tmp scripts to $HOME/"script"
* Once the node is provided, access the node interactively via ssh
* Run provided JUBE config on node
* Cleanup cancels job

#### Variables

* JUBE usage
* JUBE parameters/stages

## Visualization

* Contact Thomas Gruber for template (GitLab Pages, etc.)
* Contact Tim Gerrits may have some meta Ideas for tools and templates

## Logging

* Create logs for admins

### Content

* GitLab user
* GitLab project
* (Cluster account) 
* CI Script
* Environment variables
* Slurm Job id
* Time stamp

### Approach

* Restricted log files
* Data base for full content

## Connect GitLab User to Cluster User

* Create private data base only the runner account can access, e.g., via ssh key, pass phrase etc. 
* Map GitLab project id and user id to cluster id
* Confirm cluster account:
  * access via ssh key, (private key must be stored in DB)
  * accept ping from cluster account
  * Sign a physical form and have the setup done by CI admins
  * (build upon idm/regapp still under development)
  * Only support service accounts at first, until idm/regapp is cleared up













































