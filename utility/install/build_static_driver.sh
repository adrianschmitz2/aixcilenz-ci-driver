#!/usr/bin/bash

EXE_NAME=driver
: "${TMP:=/tmp}"
WORKPATH=$(mktemp -d -p "${TMP}")

function cleanup()
{
    rm -r $WORKPATH dist ${EXE_NAME}.spec
}

NAME='static_${EXE_NAME}.exe'
if [ "$#" -ge 1 ]; then
    NAME="$1"
fi

pyinstaller --onefile --clean --strip --workpath $WORKPATH ${STATIC_ADD_DATA} ${EXE_NAME}.py
staticx dist/$EXE_NAME $NAME
trap cleanup EXIT TERM INT HUP
